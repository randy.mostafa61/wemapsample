import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PinPointwidget extends StatelessWidget {
  final Map<String, dynamic> pinpoint;

  const PinPointwidget({Key? key, required this.pinpoint}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pattern = RegExp(r'<p class="wemap-template-simple">(.*?)<\/p>');
    final match = pattern.firstMatch(pinpoint["description"]);
    if (match != null) {
      pinpoint["description"] = match.group(1);
    }

    return Row(
      children: [
        Container(
            padding: const EdgeInsets.all(15),
            child: Image.network(pinpoint["image_url"])),
        Expanded(
            child: Column(
          children: [
            Text(
              pinpoint["name"],
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 5),
            Text(pinpoint["description"], maxLines: 2),
            Text(
              pinpoint["address"],
              style: const TextStyle(color: CupertinoColors.inactiveGray),
            )
          ],
        )),
        IconButton(onPressed: () {}, icon: const Icon(Icons.favorite_border))
      ],
    );
  }
}
