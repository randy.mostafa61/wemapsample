import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wemap_sdk/flutter_wemap_sdk.dart';
import 'package:flutter_wemap_sdk/livemap_controller.dart';
import 'package:wemapsample/views/PinPointDetailsWidget.dart';
import 'package:wemapsample/views/PinPointWidget.dart';

class MyHomePage extends StatelessWidget {
  late BuildContext _context;
  late LivemapController _mapController;

  MyHomePage({super.key});

  void _onContentUpdated(List<dynamic> pinpoints) {
    Scaffold.of(_context).showBottomSheet<void>(
      (BuildContext context) {
        return Container(
            height: 300,
            child: ListView.builder(
                itemCount: pinpoints.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    child: PinPointwidget(pinpoint: Map.from(pinpoints[index])),
                    onTap: () {
                      _mapController.openPinpoint(pinpoints[index]["id"]);
                    },
                  );
                }));
      },
    );
  }

  void _onPinpointOpen(dynamic pinpoint) {
    showDialog<void>(
        context: _context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: PinPointDetailsWidget(pinpoint: Map.from(pinpoint)),
              actions: <Widget>[
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      _mapController.closePinpoint();
                    },
                    icon: const Icon(Icons.exit_to_app)),
              ]);
        });
  }

  void _onMapCreated(LivemapController mapController) {
    _mapController = mapController;
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    const Map<String, dynamic> creationParams = <String, dynamic>{
      "token": "GUHTU6TYAWWQHUSR5Z5JZNMXX",
      "emmid": 22485
    };

    return Container(
        constraints: const BoxConstraints.expand(),
        child: Livemap(
          options: creationParams,
          onMapCreated: _onMapCreated,
          onContentUpdated: _onContentUpdated,
          onPinpointOpen: _onPinpointOpen,
        ));
  }
}

class Root extends StatelessWidget {
  const Root({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text("Wemap Sample")),
        body: MyHomePage(),
      ),
    );
  }
}
