import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PinPointDetailsWidget extends StatelessWidget {
  final Map<String, dynamic> pinpoint;

  const PinPointDetailsWidget({Key? key, required this.pinpoint})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pattern = RegExp(r'<p class="wemap-template-simple">(.*?)<\/p>');
    final match = pattern.firstMatch(pinpoint["description"]);
    if (match != null) {
      pinpoint["description"] = match.group(1);
    }

    return ListView(
      children: [
        Text(
          pinpoint["name"],
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          pinpoint["address"],
        ),
        Image.network(pinpoint["image_url"]),
        Text(pinpoint["description"]),
        const SizedBox(height: 20),
        Row(
          children: const [Icon(Icons.web_sharp), Text("Site Internet")],
        ),
        Text(pinpoint["link_url"], style: const TextStyle(color: Colors.blue)),
      ],
    );
  }
}
